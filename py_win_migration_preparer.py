#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

import os
import sys
import ctypes
import logging
import argparse
from subprocess import Popen, PIPE, call


sys_encoding = sys.stdout.encoding


def set_opt_parser():
    """
    Setting CLI(command line interface) option parser
    :return: parser - option parser
    """
    parser = argparse.ArgumentParser(
        description='Prepare to migration from Windows')
    parser.add_argument('-s', '--source',
                        help='Source disks, must be divided by comma')
    parser.add_argument('-d', '--destination', help='Destination disk')
    return parser


def set_logger():
    """
    Setting global thread-safe logging object
    :return: None
    """
    global log
    logging.basicConfig(filename='migration.log', filemode='a',
                        level=logging.DEBUG,
                        format='%(asctime)s - %(levelname)s - %(message)s')

    log = logging.getLogger('root')
    log.setLevel(logging.DEBUG)
    log.info('-----------New session-------------\n\n')


def log_and_print_error(msg):
    log.error(msg)
    print msg


def validate_disk_name(disk_name):
    """
    Validating provided disk name
    :param disk_name:
    :return: valid disk name if valid disk provided else None
    """
    if not (len(disk_name) == 1 and disk_name.isalpha()):
        msg = 'Disk option error: disk must be in A-Z or a-z, ' \
              'current option is: %s' % disk_name
        log_and_print_error(msg)
        return None
    elif not os.path.exists(disk_name + ':'):
        msg = 'Disk option error: disk %s: not exist' % disk_name
        log_and_print_error(msg)
        return None
    disk_name += ':'
    return disk_name


def validate_args(source, destination):
    """
    Validating all provided CLI args
    :param source: string with comma separated disks names, for ex.: c,d,e
    :param destination: string with destination disk name, for ex.: h
    :return: True if disks in valid format else False
    """
    sources_disks = set(source.split(','))
    print sources_disks, destination
    valid_sources = tuple(validate_disk_name(source_disk)
                          for source_disk in sources_disks
                          if validate_disk_name(source_disk))

    valid_destination = validate_disk_name(destination)

    return tuple(valid_sources), valid_destination


def disk_usage(disk):
    """
    Disk usage counting function
    :param disk: disk name
    :return: tuple with usage, free, etc
    """
    if os.name == 'nt':       # Windows

        _, total, free = ctypes.c_ulonglong(), ctypes.c_ulonglong(), \
            ctypes.c_ulonglong()
        if sys.version_info >= (3,) or isinstance(disk, unicode):
            fun = ctypes.windll.kernel32.GetDiskFreeSpaceExW
        else:
            fun = ctypes.windll.kernel32.GetDiskFreeSpaceExA
        ret = fun(disk, ctypes.byref(_), ctypes.byref(total),
                  ctypes.byref(free))
        if ret == 0:
            raise ctypes.WinError()
        used = total.value - free.value
        return dict(total=total.value, used=used, free=free.value)
    else:
        raise NotImplementedError("Platform not supported")


def analize_du_df(sources, destination):
    """
    Analizing space usage on sources
    and disk free on destination disk with 10% reserve
    :return: True if free > used + 10%, else False
    """
    total_used = 0
    for disk in sources:
        total_used += disk_usage(disk)['used']

    dest_df = disk_usage(destination)['free']
    msg = 'Total used on sources: %d, free on destination: %d' % \
          (total_used, dest_df)
    log.info(msg)
    if int(total_used * 1.1) < dest_df:
        return True
    return False


def call_system_cmd(cmd):
    """
    Calling cmd in system with logging
    :param cmd: command to call as string with params
    :return: result: error on sys.exit(1) or output on sys.exit(0)
    """
    log.info('Starting command: %s' % cmd)
    try:
        proc = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        proc.wait()
        output, error = proc.communicate()
        output = output.decode(sys_encoding)
        error = error.decode(sys_encoding)
        if proc.returncode:
            log_and_print_error('System call with cmd %s exited with error %s'
                                % (cmd, error))
            return error
        log.info('System call with cmd %s exited without errors, output %s'
                 % (cmd, output))
        return output
    except OSError as e:
        log_and_print_error('Execution of "%s" failed: %s' % (cmd, e))


def uninstall_vbx_tools():
    """
    Subj
    :return:
    """
    result = call_system_cmd("wmic product where name='VirtualBox Guest Tools'"
                             " call uninstall /nointeractive")
    return result


def uninstall_vmware_tools():
    """
    Subj
    :return:
    """
    result = call_system_cmd("wmic product where name='VMWare tools' call "
                             "uninstall /nointeractive")
    return result


def clear_hardware():
    """
    Import migration
    :return:
    """
    result = call_system_cmd('regedit /s migration.reg')
    return result


def unzip_drivers():
    """
    Try to extract for win before vista compatibility
    On modern systems this drivers already in drivers dir
    :return:
    """
    drivers = ('atapi.sys',
               'intelide.sys',
               'pciide.sys',
               'pciidex.sys',
               )
    cmd = 'expand "C:\\Windows\\Driver Cache\\i386\\Driver.cab" -F:%s ' \
          'C:\\Windows\\system32\\drivers\\'
    result = []
    for driver in drivers:
        result.append(call_system_cmd(cmd % driver))
    return '\n'.join(result)


def make_job_without_imaging():
    """
    Job package on non provided sources and destination
    :return: None
    """
    print uninstall_vbx_tools()
    print uninstall_vmware_tools()
    print clear_hardware()
    print unzip_drivers()


def image_sources(sources_disks, destination_disk):
    """
    Sysinternals suite based binary call
    :param sources_disks: subj
    :param destination_disk: subj
    :return:
    """
    print 'Snapshoting, don\'t close anything, wait for a complete...'
    result = call_system_cmd('disk2vhd -h %s %s\\snapshot.vhd -accepteula' %
                             (' '.join(sources_disks), destination_disk))
    return result


def shutdown():
    """
    Shutdowning target machine
    :return:
    """
    msg = 'All complete, shutdowning after 10 sec.'
    log.info(msg)
    print msg
    #call('shutdown /s /f /t 10', shell=True)


def main():
    args = set_opt_parser().parse_args()
    set_logger()

    if not(args.source and args.destination):
        log.warning('Source or destination not provided')
        make_job_without_imaging()
        shutdown()

    else:
        sources_disks, destination_disk = validate_args(args.source,
                                                        args.destination)

        print sources_disks, destination_disk
        if not sources_disks:
            msg = 'No valid source disks provided'
            log_and_print_error(msg)
            return
        elif not destination_disk:
            msg = 'No valid destination disk provided'
            log_and_print_error(msg)
            return
        elif destination_disk in sources_disks:
            msg = 'Destination disk must be not same as one of sources'
            log_and_print_error(msg)
            return

        if not analize_du_df(sources_disks, destination_disk):
            msg = 'There is not enough space on destination disk %s' \
                  % destination_disk
            log_and_print_error(msg)

        make_job_without_imaging()
        image_sources(sources_disks, destination_disk)
        shutdown()


if __name__ == '__main__':
    raise Exception
    main()


